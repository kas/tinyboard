#ifndef LIGHTS_H__
#define LIGHTS_H__ 1

#define N_PWMLED_MODES 3

/* logging.c */
#ifdef USE_LOGGING
void init_log();
void log_set_state(unsigned char val);
void log_flush();
void log_byte(unsigned char byte);
void log_word(uint16_t word);
#else
void inline init_log() { }
void inline log_set_state(unsigned char val) { }
void inline log_flush() { }
void inline log_byte(unsigned char byte) { }
void inline log_word(uint16_t word) { }
#endif

/* serial.c */
#define STRIP_SIZE 10
void init_serial();
void zero_frame();
void end_frame();
void send_rgb(unsigned char r, unsigned char g, unsigned char b);

#endif /* !LIGHTS_H__ */


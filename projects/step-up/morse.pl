#!/usr/bin/perl -w

my $code = shift @ARGV;

$code =~ s/\./10000/g;
$code =~ s/-/1110000/g;
$code =~ s/\//000000/g;

my $ones = ($code =~ y/1/1/);
my $len = length $code;

if ($len % 8 != 0) {
	$code .= '0' x (8 - ($len % 8));
}

$code =~ s/(.{8})/\t0b$1,\n/g;

my $ratio = sprintf('%.1f', 100*$ones/$len);
print "\t/* $len bits, $ratio\% on */\n", $code;

#ifndef LIGHTS_H__
#define LIGHTS_H__ 1

#define N_PWMLED_MODES 3

/* logging.c */
#ifdef USE_LOGGING
void init_log();
void log_set_state(unsigned char val);
void log_flush();
void log_byte(unsigned char byte);
void log_word(uint16_t word);
#else
void inline init_log() { }
void inline log_set_state(unsigned char val) { }
void inline log_flush() { }
void inline log_byte(unsigned char byte) { }
void inline log_word(uint16_t word) { }
#endif

/* adc.c */
#define PWMLED_ADC_SHIFT 1 /* 1<<1 measurements per single callback */
extern volatile unsigned char need_battery_adc;
extern volatile unsigned char need_pwmled_adc;
extern volatile unsigned char adc_enabled;
void init_adc();
void susp_adc();
void start_next_adc();

/* pwm.c */
#define PWM_MAX 0xFF
extern volatile unsigned char pwm_enabled;
void init_pwm();
void susp_pwm();
void pwm_off();
void pwm_set(uint8_t stride);

/* pwmled.c */
void init_pwmled();
void pwmled_adc(uint16_t adcval);
void pwmled_set_target(unsigned char mode);
void pwmled_on_off(unsigned char on);

/* pattern.c */
void init_pattern();
void patterns_next_tick();
void led_set_pattern(unsigned char led, unsigned char bits_len,
	unsigned char bits_start, unsigned char *data);
void led_set_number_pattern(unsigned char led,
	unsigned char num, unsigned char inv);
void pattern_reload();

/* buttons.c */
void init_buttons();
void susp_buttons();
void timer_check_buttons();
unsigned char buttons_wait_for_release();
void status_led_on_off(unsigned char on);

/* battery.c */
void battery_adc();
void init_battery();
unsigned char battery_gauge();

/* control.c */
void init_control();
void long_press_start();
void long_press();
void short_press();
void brake_on();
void brake_off();
void pwmled_pattern_select(unsigned char led);
void status_led_pattern_select(unsigned char led);
#define ERR_BATTERY 1
#define ERR_PWMLED  2
void set_error(unsigned char err);

/* wdt.c */
extern volatile uint16_t jiffies;
void init_wdt();
void susp_wdt();

/* main.c */
void power_down();

#endif /* !LIGHTS_H__ */


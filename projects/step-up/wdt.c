#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>

#include "lights.h"

void init_wdt()
{
	WDTCR = _BV(WDIE) | _BV(WDP1); // interrupt mode, 64 ms
}

void susp_wdt()
{
	wdt_disable();
}

ISR(WDT_vect) {
	++jiffies;

	if (pwm_enabled) {
		need_pwmled_adc = 1;
	}

	if (jiffies & 0x000F) {
		need_battery_adc = 1; // about every 1s
	}

	patterns_next_tick();
	timer_check_buttons();
	if (!adc_enabled)
		start_next_adc(); // only if not running
}

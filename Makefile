WEB_DIR=/home/kas/html/tinyboard/
WEB_FILES=pcb-back.png pcb-front.png schematics.png schematics-power.png \
	schematics-pin.png
GERBER_FILES=\
	tinyboard-panel.top.gbr \
	tinyboard-panel.bottom.gbr \
	tinyboard-panel.topmask.gbr \
	tinyboard-panel.bottommask.gbr \
	tinyboard-panel.topsilk.gbr \
	tinyboard-panel.bottomsilk.gbr \
	tinyboard-panel.plated-drill.cnc

all:
	@echo "Usage: make [clean|web|gerber|fab|tinyboard.pcb]"

clean:
	rm -f tinyboard.cmd tinyboard.jpg tinyboard.net tinyboard.new.pcb tinyboard.pcb- \
		tinyboard.png tinyboard.ps tinyboard.sch~ pcb-back.png pcb-front.png \
		schematics.png *.gbr *.cnc

web: $(WEB_FILES)
	test -d $(WEB_DIR) && install -m 644 $(WEB_FILES) $(WEB_DIR)

tinyboard.pcb: tinyboard.sch tinyboard.prj
	gsch2pcb tinyboard.prj

pcb-front.png: tinyboard.pcb
	pcb -x png --dpi 200 --photo-mode tinyboard.pcb \
		&& mv tinyboard.png $@

pcb-back.png: tinyboard.pcb
	pcb -x png --dpi 200 --photo-mode --photo-flip-y tinyboard.pcb \
		&& mv tinyboard.png $@

schematics.png: tinyboard.sch
	gschem -s image.scm tinyboard.sch -o schematics.png

schematics-power.png: schematics.png
	convert -crop 160x480+60+120 schematics.png $@

schematics-pin.png: schematics.png
	convert -crop 570x218+400+90 schematics.png $@

gerber: $(GERBER_FILES)

$(GERBER_FILES): tinyboard-panel.pcb
	pcb -x gerber tinyboard-panel.pcb

fab: $(GERBER_FILES)
	@sh -c 'D=tinyboard-`date +\%Y\%m\%d-\%H\%M` && mkdir $$D && \
		cp tinyboard-panel.top.gbr $$D/tinyboard.gtl && \
		cp tinyboard-panel.bottom.gbr $$D/tinyboard.gbl && \
		cp tinyboard-panel.topmask.gbr $$D/tinyboard.gts && \
		cp tinyboard-panel.bottommask.gbr $$D/tinyboard.gbs && \
		cp tinyboard-panel.topsilk.gbr $$D/tinyboard.gto && \
		cp tinyboard-panel.bottomsilk.gbr $$D/tinyboard.gbo && \
		cp tinyboard-panel.plated-drill.cnc $$D/tinyboard.txt && \
		zip -r $$D.zip $$D && \
		echo "Done. Output in $$D.zip."; \
		echo "You may also want to do git tag gerber-fab-$$D" '

.PHONY: gerber
